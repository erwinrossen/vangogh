import math
import random

from common.canvas import Canvas
from common.type_hinting import Coordinate, Color


# TODO: Make usable for non-square images
class Brushifier:
    def __init__(self):
        self.canvas = Canvas(size=(1000, 1000))

    def from_photo(self, image_name: str, output_image_name: str = None):
        assert output_image_name != image_name, 'The output image name must be different from the input image name'

        mask_canvas = Canvas(image_name=image_name)

        nr_r_divs = 200
        for r_index in range(nr_r_divs):
            r = math.sqrt(2) * r_index / nr_r_divs
            nr_phi_divs = r_index + 2
            for phi_index in range(nr_phi_divs):
                phi = phi_index / nr_phi_divs * 2 * math.pi
                x = r * math.cos(phi)
                y = r * math.sin(phi)
                pos = (x, y)

                abs_pos = mask_canvas.coord_to_coord_on_canvas(pos)
                color = mask_canvas.img.getpixel(abs_pos)
                self._stroke_from_coord(pos, color)

        self.canvas.img.show()

        if output_image_name:
            # TODO: Fix directory to save to
            self.canvas.img.save(output_image_name)

    def _stroke_from_coord(self, coord: Coordinate, color: Color):
        x, y = coord
        r = math.sqrt(x * x + y * y)
        if r < 1E-3:
            # Avoid ZeroDivisionError
            return
        phi = math.atan2(y, x)

        brush_stroke = 0.1
        brush_stroke_randomizer = 0.2
        new_phi = phi + brush_stroke / r ** 0.8 + random.random() * brush_stroke_randomizer
        new_r = r * (1 - random.random() * brush_stroke_randomizer)
        new_x = new_r * math.cos(new_phi)
        new_y = new_r * math.sin(new_phi)

        self.canvas.draw_stroke(start=coord, end=(new_x, new_y), color=color)
