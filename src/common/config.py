"""
Commont (default) configuration for all Van Gogh projects
"""

DEFAULT_OUTPUT_EXTENSION = 'jpg'
DEFAULT_OUTPUT_WIDTH = 1000
MAX_GRID_SIZE = 100
