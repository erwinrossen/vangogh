from typing import Tuple

Color = Tuple[int, int, int]
Size = Tuple[int, int]
X = float  # With float value between -1 and 1
Y = float  # With float value between -1 and 1
Coordinate = Tuple[X, Y]
XOnCanvas = float  # With float value between 0 and width
YOnCanvas = float  # With float value between 0 and height
CoordinateOnCanvas = Tuple[XOnCanvas, YOnCanvas]
