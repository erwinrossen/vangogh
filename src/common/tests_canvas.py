from unittest import TestCase
from unittest.mock import patch

# from PIL import ImageDraw
from PIL.ImageDraw import ImageDraw, Draw

from common.canvas import Canvas


class CanvasTestCase(TestCase):
    def test_draw_circle(self):
        canvas = Canvas(size=(100, 128))
        with patch.object(ImageDraw, 'ellipse') as mock_ellipse:
            canvas.draw_circle(pos=(0.25, -0.25), r=5, color=(127, 127, 127))
        # Center = (62, 48) --> Bounding box = (62-5, 48-5, 62+5, 48+5)
        mock_ellipse.assert_called_once_with((57, 43, 67, 53),
                                             fill=(127, 127, 127), outline=None)

    def test_draw_polygon(self):
        canvas = Canvas(size=(100, 128))
        with patch.object(ImageDraw, 'polygon') as mock_polygon:
            canvas.draw_polygon((0, 0.5), (0.5, 0), (0, -0.5), (-0.5, 0), color=(127, 127, 127))
        mock_polygon.assert_called_once_with([(50, 96), (75, 64), (50, 32), (25, 64)],
                                             fill=(127, 127, 127), outline=None)

    def test_draw_stroke(self):
        canvas = Canvas(size=(100, 128))
        with patch.object(ImageDraw, 'ellipse') as mock_ellipse, patch.object(ImageDraw, 'polygon') as mock_polygon:
            canvas.draw_stroke((0.5, -0.5), (-0.5, 0.5), color=(127, 127, 127), stroke_width=10)
        # Start = (25,96), end = (75,32)
        self.assertEqual(2, mock_ellipse.call_count)
        mock_ellipse.assert_any_call((20, 91, 30, 101), fill=(127, 127, 127), outline=None)
        mock_ellipse.assert_any_call((70, 27, 80, 37), fill=(127, 127, 127), outline=None)
        mock_polygon.assert_called_once_with([(71, 28), (78, 35), (28, 99), (21, 92)],
                                             fill=(127, 127, 127), outline=None)

    def test_rel_x_to_abs_x(self):
        canvas = Canvas(size=(100, 128))
        self.assertEqual(0, canvas._rel_x_to_abs_x(-1.1))
        self.assertEqual(0, canvas._rel_x_to_abs_x(-1))
        self.assertEqual(25, canvas._rel_x_to_abs_x(-0.5))
        self.assertEqual(37, canvas._rel_x_to_abs_x(-0.25))
        self.assertEqual(50, canvas._rel_x_to_abs_x(0))
        self.assertEqual(62, canvas._rel_x_to_abs_x(0.25))
        self.assertEqual(75, canvas._rel_x_to_abs_x(0.5))
        self.assertEqual(99, canvas._rel_x_to_abs_x(1))
        self.assertEqual(99, canvas._rel_x_to_abs_x(1.1))

    def test_rel_y_to_abs_y(self):
        canvas = Canvas(size=(100, 128))
        self.assertEqual(0, canvas._rel_y_to_abs_y(-1.1))
        self.assertEqual(0, canvas._rel_y_to_abs_y(-1))
        self.assertEqual(32, canvas._rel_y_to_abs_y(-0.5))
        self.assertEqual(48, canvas._rel_y_to_abs_y(-0.25))
        self.assertEqual(64, canvas._rel_y_to_abs_y(0))
        self.assertEqual(80, canvas._rel_y_to_abs_y(0.25))
        self.assertEqual(96, canvas._rel_y_to_abs_y(0.5))
        self.assertEqual(127, canvas._rel_y_to_abs_y(1))
        self.assertEqual(127, canvas._rel_y_to_abs_y(1.1))
