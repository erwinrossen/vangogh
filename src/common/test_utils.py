from typing import TypeVar, List, Set
from unittest import TestCase

T = TypeVar('T')


class AlmostEqualMixin(TestCase):
    def assertListAlmostEqual(self, expected: List[T], result: List[T], places: int = 2):
        for expected_point, result_point in zip(expected, result):
            self.assertAlmostEqual(expected_point[0], result_point[0], places=places)
            self.assertAlmostEqual(expected_point[1], result_point[1], places=places)

    def assertSetAlmostEqual(self, expected: Set[T], result: Set[T], places: int = 2):
        return self.assertListAlmostEqual(sorted(expected), sorted(result), places=places)
