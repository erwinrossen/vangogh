import argparse
from typing import Tuple

from brushify.brushify import Brushifier
from common.config import DEFAULT_OUTPUT_EXTENSION, DEFAULT_OUTPUT_WIDTH
from pointillize.pointillizer import IMAGE_EXTENSIONS

help_output_width = 'Width of the output image in pixels. The height is determined by the ratio of the input image. ' \
                    f'Optional, if not given, the default is used: {DEFAULT_OUTPUT_WIDTH}'
help_extension = f'The extension of the output images. ' \
                 f'Valid extensions are: {IMAGE_EXTENSIONS}. ' \
                 f'Optional, if not given, the default is used: {DEFAULT_OUTPUT_EXTENSION}'


def parse_args() -> Tuple[int, str]:
    parser = argparse.ArgumentParser()
    parser.description = 'Turn your pictures into marvels of wonder from the Dutch master Vincent van Gogh himself ' \
                         'using his characteristic stroke style'
    parser.add_argument('--output_width', type=int, help=help_output_width)
    parser.add_argument('--output_extension', type=str, help=help_extension)
    args = parser.parse_args()

    # Fill in defaults for missing arguments
    if not args.output_width:
        args.output_width = DEFAULT_OUTPUT_WIDTH
    if not args.output_extension:
        args.output_extension = DEFAULT_OUTPUT_EXTENSION

    return args.output_width, args.output_extension


if __name__ == '__main__':
    # TODO: Unify file management between pointillize and brushify
    _output_width, _output_extension = parse_args()
    vincent = Brushifier()
    vincent.from_photo('sana_erwin_original.jpg', 'sana_erwin_original_brush.jpg')
    vincent.from_photo('sana_erwin_crop.jpg', 'sana_erwin_crop_brush.jpg')

