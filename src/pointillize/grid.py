import math
import random
from enum import Enum
from typing import List

from common.type_hinting import Coordinate


class GridType(Enum):
    HEX = 'hex'
    SQUARE = 'square'
    RANDOM = 'random'


def hex_grid(grid_size: int, shuffle: bool = True) -> List[Coordinate]:
    """
    Return a list of coordinates on a hexagonal grid

        __
     __/  \__
    /  \__/  \
    \__/  \__/
    /  \__/  \
    \__/  \__/
       \__/

    Note: In principle, we should return coordinates within the box [-1, 1]. However, we also
    return coordinates a distance 2/grid_size away from this, in order to also fill pixels near
    the edges (center outside the box, but pixel itself visible within the box)

    :param grid_size: The number of points on the grid in one dimension
    :param shuffle: Flag whether to shuffle the coordinates (True), or keep them ordered (False)
    :return: List of coordinates
    """

    result = []
    ei = (1, 0)  # Base vector in horizontal direction
    ej = (math.cos(math.radians(60)), math.sin(math.radians(60)))  # Base vector at 60 degrees
    limit = 1 + 2 / grid_size
    for i in range(-2 * grid_size, 2 * grid_size + 1):
        for j in range(-2 * grid_size, 2 * grid_size + 1):
            x = (ei[0] * i + ej[0] * j) / grid_size
            y = (ei[1] * i + ej[1] * j) / grid_size
            if -limit <= x < limit and -limit <= y < limit:
                result.append((x, y))
    if shuffle:
        random.shuffle(result)
    return result


def square_grid(grid_size: int, shuffle=True) -> List[Coordinate]:
    """
    Return a list of coordinates on a square grid

    :param grid_size: The number of points on the grid in one dimension
    :param shuffle: Flag whether to shuffle the coordinates (True), or keep them ordered (False)
    :return: List of coordinates
    """

    result = [
        (x / (2 * grid_size), y / (2 * grid_size))
        for x in range(-2 * grid_size, 2 * grid_size + 1)
        for y in range(-2 * grid_size, 2 * grid_size + 1)
    ]
    if shuffle:
        random.shuffle(result)
    return result


def random_grid(grid_size: int, shuffle=True) -> List[Coordinate]:
    """
    Return a list of coordinates on a semi-randomized grid

    Semi-randomized means here that we use a square grid as default, but add a random part to each pixel
    Rationale: this gives us a better coverage of the entire image, while keeping the randomness to the eye

    :param grid_size: The number of points on the grid in one dimension
    :param shuffle: Flag whether to shuffle the coordinates (True), or keep them ordered (False)
    :return: List of coordinates
    """

    factor = int(math.ceil(math.sqrt(grid_size)))
    random_part = [
        (random.uniform(-1, 1), random.uniform(-1, 1))
        for _ in range(factor * grid_size * grid_size)
    ]
    return square_grid(grid_size=grid_size, shuffle=shuffle) + random_part
