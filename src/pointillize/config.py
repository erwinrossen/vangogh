"""
(Default) configuration for Van Gogh's Pointillizer project
"""

from pointillize.grid import GridType

DEFAULT_GRID_SIZES = [1, 2, 5, 10, 15, 20, 25, 50, 75]
DEFAULT_GRID_TYPES = [GridType.HEX.value, GridType.SQUARE.value, GridType.RANDOM.value]
