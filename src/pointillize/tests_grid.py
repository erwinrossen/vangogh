from unittest import TestCase

from common.test_utils import AlmostEqualMixin
from pointillize.grid import hex_grid, square_grid


class GridTestCase(AlmostEqualMixin, TestCase):
    def test_hex_grid_returns_hexagonal_grid(self):
        """
            __
         __/  \__
        /  \__/  \
        \__/  \__/
        /  \__/  \
        \__/  \__/
           \__/
        """

        grid = hex_grid(grid_size=1, shuffle=False)
        expected_grid = [(-3.00, -1.73), (-2.50, -0.87), (-2.00, 0.00), (-1.50, 0.87), (-1.00, 1.73), (-2.00, -1.73),
                         (-1.50, -0.87), (-1.00, 0.00), (-0.50, 0.87), (0.00, 1.73), (-1.00, -1.73), (-0.50, -0.87),
                         (0.00, 0.00), (0.50, 0.87), (1.00, 1.73), (0.00, -1.73), (0.50, -0.87), (1.00, 0.00),
                         (1.50, 0.87), (2.00, 1.73), (1.00, -1.73), (1.50, -0.87), (2.00, 0.00), (2.50, 0.87)]
        self.assertListAlmostEqual(expected_grid, grid)

        shuffled_grid = hex_grid(grid_size=1, shuffle=True)
        self.assertNotEqual(shuffled_grid, expected_grid)
        self.assertSetAlmostEqual(set(shuffled_grid), set(expected_grid))

    def test_square_grid_returns_overlapping_square_grid(self):
        grid = square_grid(grid_size=1, shuffle=False)
        expected_grid = [(-1.00, -1.00), (-1.00, -0.50), (-1.00, 0.00), (-1.00, 0.50), (-1.00, 1.00), (-0.50, -1.00),
                         (-0.50, -0.50), (-0.50, 0.00), (-0.50, 0.50), (-0.50, 1.00), (0.00, -1.00), (0.00, -0.50),
                         (0.00, 0.00), (0.00, 0.50), (0.00, 1.00), (0.50, -1.00), (0.50, -0.50), (0.50, 0.00),
                         (0.50, 0.50), (0.50, 1.00), (1.00, -1.00), (1.00, -0.50), (1.00, 0.00), (1.00, 0.50),
                         (1.00, 1.00)]
        for expected_point, point in zip(expected_grid, grid):
            self.assertAlmostEqual(expected_point[0], point[0], places=2)
            self.assertAlmostEqual(expected_point[1], point[1], places=2)

        shuffled_grid = square_grid(grid_size=1, shuffle=True)
        self.assertNotEqual(shuffled_grid, expected_grid)
        self.assertSetEqual(set(shuffled_grid), set(expected_grid))
