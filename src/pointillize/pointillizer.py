import os
from functools import cached_property
from typing import List

from PIL import Image, ImageDraw

from common.config import DEFAULT_OUTPUT_WIDTH, DEFAULT_OUTPUT_EXTENSION, MAX_GRID_SIZE
from common.type_hinting import Coordinate, Color, CoordinateOnCanvas
from pointillize.grid import GridType, random_grid, square_grid, hex_grid

IMAGE_EXTENSIONS = {'jpg', 'jpeg', 'png'}


class Pointillizer:
    """
    Paint like the famous Dutch artist!
    """

    _pointillizer_dir = os.path.dirname(__file__)
    _src_dir = os.path.dirname(_pointillizer_dir)
    _root_dir = os.path.dirname(_src_dir)
    _img_dir = os.path.join(_root_dir, 'img')
    _input_dir = os.path.join(_img_dir, 'input')
    _output_dir = os.path.join(_img_dir, 'output')

    grid_size: int  # Number of points in one direction (up to a constant)
    output_width: int  # Number of pixels of the output image
    output_extension: str  # Extension of the output file

    _input_img: Image  # The image that serves as template
    _img: Image  # The image we are drawing on
    _draw: ImageDraw

    def __init__(self, output_width: int = DEFAULT_OUTPUT_WIDTH, output_extension: str = DEFAULT_OUTPUT_EXTENSION):
        assert output_extension in IMAGE_EXTENSIONS, f'Extension {output_extension} is in the ' \
                                                     f'valid extensions: {IMAGE_EXTENSIONS}'

        self.grid_size = 1
        self.output_width = output_width
        self.output_extension = output_extension

    @cached_property
    def size(self):
        return self._img.size

    @cached_property
    def width(self) -> int:
        return self.size[0]

    @cached_property
    def height(self):
        return self.size[1]

    def get_input_images(self, input_dir: str = None) -> List[str]:
        """
        Return a list of filenames of image files in the input directory

        :param input_dir: Optional, directory to search in. Default: img/input
        """

        search_path = os.path.join(input_dir or self._input_dir)
        list_imgs = [f for f in os.listdir(search_path)
                     if os.path.isfile(os.path.join(search_path, f))
                     and os.path.splitext(os.path.join(search_path, f))[-1].replace('.', '') in IMAGE_EXTENSIONS]
        return list_imgs

    def set_input_img(self, input_file: str, input_dir: str = None):
        """
        Open an image from a file in the img/input directory

        :param input_file: Filename including extension to set as original image
        :param input_dir: Optional, directory where the file is located. Default: img/input
        """

        full_filename_in = os.path.join(input_dir or self._input_dir, input_file)
        self._input_img = Image.open(full_filename_in)

    def set_grid_size(self, grid_size: int):
        """
        Set the grid size for the output image, defined as the number of pixels in one direction (up to a constant)
        """

        assert grid_size <= MAX_GRID_SIZE, f'Grid sizes larger than {MAX_GRID_SIZE} are not supported'
        self.grid_size = grid_size

    def pointillize_img(self, grid_type: GridType):
        width = self.output_width
        height = int(round(width * self._input_img.size[1] / self._input_img.size[0]))
        self._init_img(width, height)
        radius = self.width / (4 * self.grid_size * 1.05)

        if grid_type == GridType.HEX:
            grid = hex_grid(self.grid_size)
        elif grid_type == GridType.SQUARE:
            grid = square_grid(self.grid_size)
        elif grid_type == GridType.RANDOM:
            grid = random_grid(self.grid_size)
        else:
            raise AssertionError('Unknown grid type')

        for point in grid:
            color = self._get_pixel(self._input_img, point)
            if color is not None:
                # Calculate the point on the canvas outside _draw_circle, to be able to use width and height
                # that are already in memory at this point, so no lookups required on this object.
                x_on_canvas = (point[0] + 1) / 2 * width
                y_on_canvas = (point[1] + 1) / 2 * height
                point_on_canvas = (x_on_canvas, y_on_canvas)
                self._draw_circle(center=point_on_canvas, radius=radius, fill_color=color, outline_color=None)

    def show(self):
        """
        Show the image to screen
        """

        self._img.show()

    def save(self, img_name: str, img_dir: str = None):
        """
        Save the image in the object to file

        :param img_name: Filename without extension
        :param img_dir: Optional, directory where to save the file. Default: img/output
        """

        if img_dir is None:
            img_dir = self._output_dir

        full_img_path = os.path.join(img_dir, f'{img_name}.{self.output_extension}')
        self._img.save(full_img_path, quality=95, optimize=False)

    def _init_img(self, width: int, height: int):
        size = (width, height)
        self._img = Image.new('RGB', size, (255, 255, 255))
        self._draw = ImageDraw.Draw(self._img)

    @staticmethod
    def _get_pixel(img: Image, coordinate: Coordinate) -> Color:
        """
        Return the pixel value at a given position

        :param img: Image object of which to determine the image
        :param coordinate: The coordinate, with (x,y) within the square between (-1,-1), (-1,1), (1,1) and (1,-1)
        :returns: The pixel value as tuple
        """

        width, height = img.size
        x = int((coordinate[0] + 1) / 2 * width)
        y = int((coordinate[1] + 1) / 2 * height)

        # Due to rounding errors or coordinates just outside the box, it could be that the
        # coordinate falls outside the given image. In that case, return the closest pixel
        if not 0 <= x < width:
            x = max(0, x)
            x = min(x, width - 1)
        if not 0 <= y < height:
            y = max(0, y)
            y = min(y, height - 1)

        try:
            return img.getpixel((x, y))
        except IndexError as e:
            msg = f'Cannot get pixel {(x, y)} from image with size {img.size}'
            raise IndexError(msg) from e

    def _draw_circle(self, center: CoordinateOnCanvas, radius, fill_color, outline_color):
        left = center[0] - radius
        right = center[0] + radius
        upper = center[1] - radius
        lower = center[1] + radius
        self._draw.ellipse((left, upper, right, lower), fill=fill_color, outline=outline_color)
